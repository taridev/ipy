# demande une année et affiche s'il s'agit d'une année bisextile
try:
    while True:
        annee = int(input('année ? '))
        print((str(annee) + ' est une année '),
              'bisextile' if (annee % 4 == 0 and annee % 100 != 0) or annee % 400 == 0 else 'non bisextile')
except ValueError:
    pass
