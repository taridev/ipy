def calcule_volume_sphere(rayon):
    """
    calcule le volume d'une sphere
    :param rayon: rayon de la sphère
    :return:
    """
    return 4/3 * 3.14 * rayon ** 3
