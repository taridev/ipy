# on attend une adresse formatée comme suit : ip|cidr --- i.e: 192.168.20.34|24
address = input()

# on extrait l'ip sous la forme d'une' liste d'entiers [192, 168, 20, 34]
ip = [int(x) for x in address.split('/')[0].split('.')]

# on extrait la valeur du cidr
cidr = int(address.split('/')[1])

# passage du cidr sous forme binaire par ex: |24 -> '11111111111111111111111100000000'
mask_bin = '1' * cidr + '0' * (32 - cidr)

# récupération du mask sous forme de list d'entier : |24 -> [255, 255, 255, 0]
mask = [int(mask_bin[i:i + 8], 2) for i in range(0, len(mask_bin), 8)]

# pour trouver l'adresse de sous réseau, on applique  l'octet du masque correspondant à chaque octet de l'adresse
#   192.168. 20.34
# & 255.255.255.0
# = 192.168. 20.0 -> adresse de sous-réseau
subnet = [ip[i] & mask[i] for i in range(len(mask))]

# pour récupérer l'adresse de l'hôte, il faut isoler la partie déterminante du masque en lui appliquant xor
# sur un masque |32
#   255.255.255.255
# | 255.255.255.0
# =   0.  0.  0.255 -> partie déterminante du masque
rest = [mask[i] ^ [255, 255, 255, 255][i] for i in range(len(ip))]

# on obtient l'adresse de l'hôte à l'aide d'un ET logique entre la partie déterminante du masque et l'ip fournie
#   192.168.20.34
# &   0.  0. 0.255
# =   0.  0. 0.34
host = [rest[i] & ip[i] for i in range(len(rest))]

# pour connaitre l'adresse de broadcast on doit d'abord connaitre le masque inversé du mask de sous-réseau :
#  chaque 1 devient 0 et inversement -> ~255.255.255.0 = '00000000000000000000000011111111' = 0.0.0.255
revert_mask_bin = ''.join('0' if bit == '1' else '1' for bit in mask_bin)
# formatage du masque inversé sous forme de liste d'entiers
revert_mask = [int(revert_mask_bin[i:i + 8], 2) for i in range(0, len(revert_mask_bin), 8)]

# l'adresse de broadcast s'obtient en appliquant un OU logique entre le masque de sous-réseau et le masque inversé:
#   192.168. 20.0
# |   0.  0.  0.255
# = 192.168. 15.255
broadcast = [subnet[i] | revert_mask[i] for i in range(len(revert_mask))]

# nombre maximal d'hôtes : 2 puissance(32-cidr) - 2
host_count = 2 ** (32 - cidr) - 2

print('adresse réseau :', ip)
print('masque de sous-réseau :', mask)
print('sous-réseau :', subnet)
print('adresse de l\'hôte :', host)
print('adresse de diffusion :', broadcast)
print('nombre max d\'hôte :', host_count)
