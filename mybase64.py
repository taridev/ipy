# conversion base64 <-> ascii
# mail@thierry-decker.com
base64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'


def encode64(message_ascii):
    """
    Effectue l'encodage d'un message en ascii en message en base64
    :param message_ascii: message original en ascii
    :return: le message encodé en base 64
    """
    # transformation du message en mots de 8bits
    msg_bits = ''.join([bin(ord(c))[2:].zfill(8) for c in message_ascii])
    # transformation du message en caractères de la base 64
    message_64 = ''.join([base64[int(msg_bits[i:i + 6] + '0' * (6 - len(msg_bits[i:i + 6])), 2)]
                          for i in range(0, len(msg_bits), 6)])
    return message_64 + '=' * (4 - (len(message_64) % 4))


def decode64(message_64):
    """
    Effectue le décodage d'un message de base 64 vers ascii
    :param message_64: le message en base 64 à décoder
    :return: le message en ascii
    """
    # transformation du message en bit par, suppression du la chaine '0b' pour chaque lettre, nettoyage des signes '='
    # et suppression des paires de '0' indiqués par les signes '=' pour obtenir le message original en bits
    msg_bits = ''.join([bin(base64.find(c))[2:].zfill(6) for c in message_64.rstrip('=')])[:-2 * message_64.count('=')]
    # décodage du message en caractères
    return ''.join([chr(int(msg_bits[i:i + 8], 2)) for i in range(0, len(msg_bits), 8)])


if __name__ == '__main__':

    choice = input('(E)ncode, (D)ecode, e(X)it ? ')

    if choice.lower() in 'ed':
        entree = input('msg ? ')
        sortie = ''
        if choice.lower() == 'e':
            sortie = encode64(entree)
            print('ce qui donne en base64:', sortie)
        else:
            sortie = decode64(entree)
            print('ce qui donne en ascii:', sortie)
